from flask import Flask, jsonify, send_file, request
import json
import urllib.parse
from urllib.parse import unquote

app = Flask(__name__)

def read_json_file(filename):
    with open(filename) as file:
        data = json.load(file)
    return data

my_dict = read_json_file("10_orgs_updated_523.json")

#call organization name, return general information of that org
@app.route('/organizations/info/<name>')
def organization_info(name):
    decoded_name = urllib.parse.unquote(name)
    decoded_name = decoded_name.replace('_', ' ')  # Replace underscores with spaces
    for i in my_dict:
        if decoded_name in i['name']:
            organization = {
                'name': i['name'],
                'website': i['website'],
                'description': i['description'],
                'causes': i['causes']
            }
            return jsonify(organization)
    return jsonify({'message': 'Organization not found.'})

#call organization name, return causes
@app.route('/organizations/causes/<name>')
def organization_causes(name):
    decoded_name = urllib.parse.unquote(name)
    decoded_name = decoded_name.replace('_', ' ')
    for i in my_dict:
        if decoded_name in i['name']:
            return jsonify({'causes': i['causes']})
    return jsonify({'message': 'Error: You may have incorrectly spelled the organization\'s name, please check your spelling and try again.'})

#call cause, return relevant organizations
@app.route('/organizations/<cause>', methods=['GET'])
def get_organizations(cause):
    cause = cause.replace("_", " ")  # Replace underscores with spaces

    filtered_organizations = []

    for organization in my_dict:
        organization_copy = organization.copy()
        organization_copy.pop("contact_info", None)  # Remove the 'contact_info' field
        for org_cause in organization.get("causes", []):
            if isinstance(org_cause, dict):
                if org_cause.get("name") == cause:
                    filtered_organizations.append(organization_copy)
                    break
            elif isinstance(org_cause, str):
                if org_cause == cause:
                    filtered_organizations.append(organization_copy)
                    break

    if not filtered_organizations:
        return jsonify({"message": "No organizations found for the specified cause."}), 404

    return jsonify(filtered_organizations)

    
#download full json file
@app.route('/download/json')
def download_json():
    filename = "10_orgs_updated_523.json"
    return send_file(filename, as_attachment=True)

#download general json file
@app.route('/download/general/json')
def download_general_json():
    filename = "10_orgs_updated_523.json"
    data = []

    # Read the JSON file
    with open(filename, 'r') as file:
        data = json.load(file)

    # Extract desired fields and create a new list of dictionaries
    extracted_data = []
    for entry in data:
        extracted_entry = {
            'name': entry['name'],
            'website': entry['website'],
            'description': entry['description'],
            'causes': entry['causes']
        }
        extracted_data.append(extracted_entry)

    # Save the extracted data to a new JSON file
    extracted_filename = "extracted_data.json"
    with open(extracted_filename, 'w') as file:
        json.dump(extracted_data, file, indent=4)

    # Download the extracted JSON file
    return send_file(extracted_filename, as_attachment=True)
    
@app.route('/')
def info():
    message=f"""
       Hello, welcome to the Social Justice Organizations Directory API.<br>
       You can call the following functions to pull data:<br>
           - /organizations/info/NAME : Call the organization's name to grab the organization's general information.<br>
           - /organizations/causes/NAME : Call the organization's name to grab the organization's main causes.<br>
           - /organizations/CAUSE : Call a cause to grab relevant organizations.<br>
           - /download/general/json : Download the general JSON file.
    """ 
    return message 

